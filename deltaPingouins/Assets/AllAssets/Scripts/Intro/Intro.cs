﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Intro : MonoBehaviour {

    public Transform introScreens;
    public float fadeStep = 0.2f;
    public float fadeStepDelay = 0.2f;
    private int maxScreen;
    private int screen;
    private bool fading = true;

    void Start()
    {
        screen = 0;
        maxScreen = introScreens.transform.childCount;
        SetAllAlphas(0);
        FadeIn(screen);
    }

    void Update()
    {
        if (!fading && Input.GetButtonDown("Jump"))
        {
            if (screen + 1 == maxScreen)
            {
                FadeOut(screen);
                LoadManager.previousLevel = "Intro";
                LoadManager.LevelToLoad = "QG";
                SceneManager.LoadScene("loading");
            }
            else
            {
                fading = true;
                FadeOut(screen);
                screen++;
                FadeIn(screen);
            }
        }
    }

    void FadeIn(int indice)
    {
        CanvasGroup cv = introScreens.GetChild(indice).gameObject.GetComponent<CanvasGroup>();
        StartCoroutine(IncAlpha(cv, 1, fadeStep, fadeStepDelay));
    }
    void FadeOut(int indice)
    {
        CanvasGroup cv = introScreens.GetChild(indice).gameObject.GetComponent<CanvasGroup>();
        StartCoroutine(DecAlpha(cv, 0, fadeStep, fadeStepDelay));
    }

    IEnumerator IncAlpha(CanvasGroup cv, float targetAlpha, float step, float delay)
    {
        yield return new WaitForSeconds(delay);
        cv.alpha += step;
        if (cv.alpha < targetAlpha)
        {
            StartCoroutine(IncAlpha(cv, targetAlpha, step, delay));
        } else
        {
            fading = false;
        }
    }

    IEnumerator DecAlpha(CanvasGroup cv, float targetAlpha, float step, float delay)
    {
        yield return new WaitForSeconds(delay);
        cv.alpha -= step;
        if (cv.alpha > targetAlpha)
        {
            StartCoroutine(DecAlpha(cv, targetAlpha, step, delay));
        }
    }

    void SetAllAlphas(float targetAlpha)
    {
        for (int i = 0; i < maxScreen; i++)
        {
            introScreens.GetChild(i).gameObject.GetComponent<CanvasGroup>().alpha = targetAlpha;
            introScreens.GetChild(i).gameObject.SetActive(true);
        }
    }
}
