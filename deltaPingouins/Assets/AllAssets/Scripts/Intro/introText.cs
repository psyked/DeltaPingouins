﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class introText : MonoBehaviour {

    public Text t;

	// Use this for initialization
	void Start () {
        char delimiter = '$';
        string[] tab = t.text.Split(delimiter);
        t.text = tab[0] + LoadManager.playerName + tab[1];
	}
}
