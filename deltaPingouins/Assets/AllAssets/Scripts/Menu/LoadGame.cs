﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadGame : MonoBehaviour {

	public void OnClick ()
    {
        if (File.Exists("./saves/doc.save"))
        {
            LoadManager.LevelToLoad = "QG";
            LoadManager.previousLevel = "menu";
            SceneManager.LoadScene("loading");
        }

    }
}
