﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GoToLevelEditor : MonoBehaviour {
	public void OnClick()
    {
        LoadManager.previousLevel = "menu";
        LoadManager.LevelToLoad = "levelEditor";
        SceneManager.LoadScene("loading");
    }
}
