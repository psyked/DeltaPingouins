﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SerializedQG{

    public string playerName = "Player";
    public string icebergStatus;
    public string floeStatus;
    public string sorbets;
    public string[][] penguins;
    public string[][] missions;
    public string roomSave;
    public string[][] upgrades;
} 
