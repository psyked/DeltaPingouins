﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Upgrade {

    private UpgradeType type = UpgradeType.life;
    private int level = 0;
    private int price;
    private string description;
    
    //constructors
    public Upgrade(UpgradeType type, int price)
    {
        this.type = type;
        this.price = price;
        this.level = 0;
        SetDescription();
    }

    public Upgrade(UpgradeType type, int price, int level)
    {
        this.type = type;
        this.price = price;
        this.level = level;
        SetDescription();
    }

    private void SetDescription()
    {
        switch (this.type)
        {
            case UpgradeType.life:
                this.description = "Bonus :\n+ 1PV\npour tous vos soldats";
                break;
            case UpgradeType.dmg:
                this.description = "Bonus :\n+ 1DGT à distance\npour tous vos soldats";
                break;
            case UpgradeType.speed:
                this.description = "Bonus :\ndéplacement + 1 case/tour\npour tous vos soldats";
                break;
            default:
                this.description = "";
                break;
        }
    }

    //getters
    public UpgradeType GetUpgradeType()
    {
        return this.type;
    }

    public int GetLevel()
    {
        return this.level;
    }

    public string GetDescription()
    {
        return this.description;
    }

    public int GetPrice()
    {
        return this.price;
    }

    public void BuyUpgrade()
    {
        if (level < 5)
        {
            level++;
            price += 100;
        }
    }
}
