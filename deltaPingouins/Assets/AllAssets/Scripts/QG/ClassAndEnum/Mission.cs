﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mission {

    private string name = "";
    private MissionType type = MissionType.elimination;
    private bool done = false;
    private string map = "demo.map";
    //meteo


    // constructor
    public Mission(string name, MissionType type, string map)
    {
        this.name = name;
        this.type = type;
        this.map = map;
    }

    // constructor used only when the game is loaded
    public Mission(string name, MissionType type, string map, bool done)
    {
        this.name = name;
        this.type = type;
        this.map = map;
        this.done = done;
    }

    // getters
    public string GetName()
    {
        return this.name;
    }

    public MissionType GetMissionType()
    {
        return this.type;
    }

    public string GetMap()
    {
        return this.map;
    }

    public bool IsDone()
    {
        return this.done;
    }

    // to call when the mission is completed
    public void Finished()
    {
        this.done = true;
    }
}
