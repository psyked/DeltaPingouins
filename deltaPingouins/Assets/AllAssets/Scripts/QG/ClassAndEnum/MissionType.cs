﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MissionType
{
    elimination,
    sauvetage,
    protectionbanquise
}
