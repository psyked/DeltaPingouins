﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// A class used to defines soldiers in QG
public class Penguin {

    private string penguinName = "";
    private PenguinSpec spec = PenguinSpec.plongeur;
    private int maxPv = 0;
    private int maxMvmt = 0;
    private int dmg = 0;
    private int cac = 0;
    private bool inSquad = false;
    private bool dead = false;

    //constructors
    public Penguin(string name, PenguinSpec spec, int pv, int mvmt, int dmg, int cac)
    {
        this.penguinName = name;
        this.spec = spec;
        this.maxPv = pv;
        this.maxMvmt = mvmt;
        this.dmg = dmg;
        this.cac = cac;
        this.inSquad = false;
        this.dead = false;
    }

    public Penguin(string name, PenguinSpec spec, int pv, int mvmt, int dmg, int cac, bool inSquad, bool dead)
    {
        this.penguinName = name;
        this.spec = spec;
        this.maxPv = pv;
        this.maxMvmt = mvmt;
        this.dmg = dmg;
        this.cac = cac;
        this.inSquad = inSquad;
        this.dead = dead;
    }

    //getters
    public string GetName()
    {
        return this.penguinName;
    }

    public PenguinSpec GetSpec()
    {
        return this.spec;
    }

    public int GetMaxPv()
    {
        return this.maxPv;
    }

    public int GetMaxMvmt()
    {
        return this.maxMvmt;
    }

    public int GetDmg()
    {
        return this.dmg;
    }

    public int GetCac()
    {
        return this.cac;
    }

    public bool IsInSquad()
    {
        return this.inSquad;
    }

    public bool IsDead()
    {
        return this.dead;
    }

    //setters
    public void SetDmg(int dmg)
    {
        this.dmg = dmg;
    }

    public void SetCac(int cac)
    {
        this.cac = cac;
    }

    public void SetMaxPv(int pv)
    {
        this.maxPv = pv;
    }

    public void SetMaxMvmt(int mvmt)
    {
        this.maxMvmt = mvmt;
    }
}
