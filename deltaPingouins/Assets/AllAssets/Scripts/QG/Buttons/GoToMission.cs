﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GoToMission : MonoBehaviour
{
    public QGManager manager;
    public Button parent;

    void Start()
    {
        parent.interactable = false;
    }

    public void OnClick()
    {
        manager.SaveGame();
        List<Upgrade> upgrades = manager.GetUpgrades();
        foreach(Upgrade up in upgrades)
        {
            UpgradeType type = up.GetUpgradeType();
            if (type == UpgradeType.life)
            {
                LoadManager.lifeLevel = up.GetLevel();
            }
            else if (type == UpgradeType.speed)
            {
                LoadManager.mvmtLevel = up.GetLevel();
            }
            else if (type == UpgradeType.dmg)
            {
                LoadManager.dmgLevel = up.GetLevel();
            }
        }
        LoadManager.penguins = manager.GetSelectedPenguins();
        LoadManager.mapName = manager.GetSelectedMap();
        LoadManager.previousLevel = "QG";
        LoadManager.LevelToLoad = "mission";
        SceneManager.LoadScene("loading");
    }
}
