﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PenguinSelectButton : MonoBehaviour {

    public QGManager manager;
    public Penguin penguin;
    public Button button;

    public void OnClick()
    {
        if (manager.SelectPenguin(penguin))
        {
            button.interactable = false;
        }
    }
}
