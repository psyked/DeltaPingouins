﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MissionButtonBehaviour : MonoBehaviour {

    public QGManager manager;
    public Mission mission;

    void OnClick () {
        manager.SelectMission(mission);
    }
}
