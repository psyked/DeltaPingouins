﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CancelMissionButton : MonoBehaviour {

    public QGManager manager;

    public void OnClick()
    {
        manager.CancelMission();
    }
}
