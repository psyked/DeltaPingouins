﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeButton : MonoBehaviour {

    public QGManager manager;
    public Upgrade upgrade;
    public GameObject parent;
    public Button b;

	public void OnClick () {
        if (manager.Pay(upgrade.GetPrice()))
        {
            upgrade.BuyUpgrade();
            UpdateValues();
            if (upgrade.GetPrice() <= manager.GetSorbets())
            {
                b.interactable = true;
            }
            else
            {
                b.interactable = false;
            }
        }
    }

    private void UpdateValues()
    {
        Transform child = parent.transform.Find("Name");
        Text name = child.GetComponent<Text>();
        child = parent.transform.Find("Level");
        Text level = child.GetComponent<Text>();
        child = parent.transform.Find("Description");
        Text type = child.GetComponent<Text>();
        child = parent.transform.Find("Price");
        Text price = child.GetComponent<Text>();
        name.text = UpgradeTypeToString(upgrade.GetUpgradeType());
        level.text = "niveau " + upgrade.GetLevel().ToString() + "/5";
        type.text = upgrade.GetDescription();
        price.text = "Coût :\n" + upgrade.GetPrice() + " sorbets";
    }

    public string UpgradeTypeToString(UpgradeType r)
    {
        switch (r)
        {
            case UpgradeType.life:
                return "Vie";
            case UpgradeType.dmg:
                return "Dégats";
            case UpgradeType.speed:
                return "Vitesse";
            default:
                return "";
        }
    }
}

