﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QG {

    public string playerName = "";
    public int icebergStatus = 1;
    public int floeStatus = 95;
    public int sorbets = 0;
    public List<Penguin> penguins = new List<Penguin>();
    public List<Mission> missions = new List<Mission>();
    public Room room;
    public List<Upgrade> upgrades = new List<Upgrade>();

    public SerializedQG Serialize()
    {
        SerializedQG sQG = new SerializedQG
        {
            playerName = this.playerName,
            icebergStatus = this.icebergStatus.ToString(),
            floeStatus = this.floeStatus.ToString(),
            sorbets = this.sorbets.ToString(),
            penguins = new string[penguins.Count][],
            missions = new string[missions.Count][],
            roomSave = room.GetRoomType().ToString(),
            upgrades = new string[upgrades.Count][]
        };
        int i = 0;
        foreach (Penguin penguin in penguins)
        {
            sQG.penguins[i] = new string[8];
            sQG.penguins[i][0] = (string)penguin.GetName();
            sQG.penguins[i][1] = (string)penguin.GetSpec().ToString();
            sQG.penguins[i][2] = (string)penguin.GetMaxPv().ToString();
            sQG.penguins[i][3] = (string)penguin.GetMaxMvmt().ToString();
            sQG.penguins[i][4] = (string)penguin.GetDmg().ToString();
            sQG.penguins[i][5] = (string)penguin.GetCac().ToString();
            sQG.penguins[i][6] = (string)penguin.IsInSquad().ToString();
            sQG.penguins[i][7] = (string)penguin.IsDead().ToString();
            i++;
        }
        i = 0;
        foreach (Mission mission in missions)
        {
            sQG.missions[i] = new string[4];
            sQG.missions[i][0] = (string)mission.GetName();
            sQG.missions[i][1] = (string)mission.GetMissionType().ToString();
            sQG.missions[i][2] = (string)mission.GetMap().ToString();
            sQG.missions[i][3] = (string)mission.IsDone().ToString();
            i++;
        }
        i = 0;
        foreach (Upgrade up in upgrades)
        {
            sQG.upgrades[i] = new string[3];
            sQG.upgrades[i][0] = (string)up.GetUpgradeType().ToString();
            sQG.upgrades[i][1] = (string)up.GetPrice().ToString();
            sQG.upgrades[i][2] = (string)up.GetLevel().ToString();
            i++;
        }
        return sQG;
    }

    public void Deserialize(SerializedQG sQG, QGManager manager)
    {
        this.playerName = sQG.playerName;
        this.icebergStatus = int.Parse(sQG.icebergStatus);
        this.floeStatus = int.Parse(sQG.floeStatus);
        this.sorbets = int.Parse(sQG.sorbets);
        penguins = new List<Penguin>();
        missions = new List<Mission>();
        this.room = new Room(ToRoomType(sQG.roomSave));
        upgrades = new List<Upgrade>();
        foreach (string[] p in sQG.penguins)
        {
            Penguin pAdd = new Penguin(p[0], ToPenguinSpec(p[1]), int.Parse(p[2]), int.Parse(p[3]), int.Parse(p[4]), int.Parse(p[5]), bool.Parse(p[6]), bool.Parse(p[7]));
            penguins.Add(pAdd);
        }
        foreach (string[] m in sQG.missions)
        {
            Mission mAdd = new Mission(m[0], ToMissionType(m[1]), m[2], bool.Parse(m[3]));
            missions.Add(mAdd);
        }
        foreach (string[] u in sQG.upgrades)
        {
            Upgrade uAdd = new Upgrade(ToUpgradeType(u[0]), int.Parse(u[1]), int.Parse(u[2]));
            upgrades.Add(uAdd);
        }
    }

    public UpgradeType ToUpgradeType(string s)
    {
        UpgradeType ty;
        s.ToLower();
        switch (s)
        {
            case "life":
                ty = UpgradeType.life;
                break;
            case "dmg":
                ty = UpgradeType.dmg;
                break;
            case "speed":
                ty = UpgradeType.speed;
                break;
            default:
                ty = UpgradeType.life;
                break;
        }
        return ty;
    }

    public PenguinSpec ToPenguinSpec(string s)
    {
        PenguinSpec spe;
        s.ToLower();
        switch (s)
        {
            case "plongeur":
                spe = PenguinSpec.plongeur;
                break;
            case "soigneur":
                spe = PenguinSpec.soigneur;
                break;
            case "constructeur":
                spe = PenguinSpec.constructeur;
                break;
            case "pondeur":
                spe = PenguinSpec.pondeur;
                break;
            default:
                spe = PenguinSpec.plongeur;
                break;
        }
        return spe;
    }

    public MissionType ToMissionType(string s)
    {
        MissionType type;
        s.ToLower();
        switch (s)
        {
            case "elimination":
                type = MissionType.elimination;
                break;
            case "sauvetage":
                type = MissionType.sauvetage;
                break;
            case "protectionbanquise":
                type = MissionType.protectionbanquise;
                break;
            default:
                type = MissionType.elimination;
                break;
        }
        return type;
    }

    public RoomType ToRoomType(string s)
    {
        RoomType type;
        s.ToLower();
        switch (s)
        {
            case "onsale":
                type = RoomType.onsale;
                break;
            case "coffemachine":
                type = RoomType.coffemachine;
                break;
            case "icecreamfactory":
                type = RoomType.icecreamfactory;
                break;
            default:
                type = RoomType.onsale;
                break;
        }
        return type;
    }
}
