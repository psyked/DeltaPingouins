﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class QGManager : MonoBehaviour {

    private static string playerName = "player";
    private static QG qg = new QG();

    // infos texts
    public Text iceberg;
    public Text floe;
    public Text sorbets;

    private bool isInPopUp = false;
    public GameObject popupCanvas;
    public GameObject endMissionCanvas;
    public GameObject qgCanvas;
    public RecruitListBehaviour recruitList;
    public MissionListBehaviour missionList;
    public SquadListBehaviour squadList;
    public ForSaleListBehaviour roomList;
    public UpgradeListBehaviour upgradeList;
    public FallenListBehaviour fallenList;

    private Mission selectedMission;
    public Button chooseButton;
    private List<Penguin> selectedPenguins = new List<Penguin>();
    public Button playButton;

    private void Start()
    {
        if (LoadManager.previousLevel == "Intro")
        {
            NewGame(LoadManager.playerName);
        }
        else if (LoadManager.previousLevel == "menu")
        {
            LoadGame();
        }
        else if (LoadManager.previousLevel == "mission")
        {
            EndMission();
        }
        else
        {
            NewGame(LoadManager.playerName);
        }
        UpdateInfos();
        InitComponents();
    }

    // manage pop up
    void Update()
    {
        if (isInPopUp)
            return;
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            popupCanvas.SetActive(true);
            qgCanvas.SetActive(false);
            isInPopUp = true;
        }
    }

    // close pop up
    public void BackOnQG()
    {
        endMissionCanvas.SetActive(false);
        popupCanvas.SetActive(false);
        qgCanvas.SetActive(true);
        isInPopUp = false;
    }

    private void InitComponents()
    {
        recruitList.Init();
        missionList.Init();
        squadList.Init();
        roomList.Init();
        upgradeList.Init();
        fallenList.Init();
    }

    public void NewGame(string player)
    {
        playerName = player;
        qg.playerName = player;
        qg.icebergStatus = 1;
        qg.floeStatus = 95;
        qg.sorbets = 60;
        qg.penguins.Add(new Penguin(player, PenguinSpec.plongeur, 7, 4, 4, 4, true, true));
        qg.penguins.Add(new Penguin("Ada Lovelace", PenguinSpec.plongeur, 7, 4, 4, 4, true, false));
        qg.penguins.Add(new Penguin("Bob Marley", PenguinSpec.soigneur, 6, 4, 3, 3, true, false));
        qg.penguins.Add(new Penguin("Natalia Alianovna", PenguinSpec.constructeur, 10, 4, 5, 5, true, false));
        qg.penguins.Add(new Penguin("Bruno Galiardo", PenguinSpec.pondeur, 7, 4, 4, 4, true, false));
        qg.penguins.Add(new Penguin("Edward Nygma", PenguinSpec.plongeur, 7, 4, 4, 4, false, false));
        qg.penguins.Add(new Penguin("David Bowie", PenguinSpec.soigneur, 6, 4, 3, 3, false, false));
        qg.penguins.Add(new Penguin("Alexandre le grand", PenguinSpec.constructeur, 10, 4, 5, 5, false, false));
        qg.penguins.Add(new Penguin("Marie Antoinette", PenguinSpec.pondeur, 7, 4, 4, 4, false, false));
        qg.missions.Add(new Mission("Tout commence", MissionType.elimination, "demo.map"));
        qg.room = new Room();
        qg.upgrades.Add(new Upgrade(UpgradeType.life, 50));
        qg.upgrades.Add(new Upgrade(UpgradeType.speed, 200));
        qg.upgrades.Add(new Upgrade(UpgradeType.dmg, 400));
    }

    public void LoadGame()
    {
        if (File.Exists("./saves/doc.save"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open("./saves/doc.save", FileMode.Open);
            qg.Deserialize((SerializedQG)bf.Deserialize(file), this);
            file.Close();
            playerName = qg.playerName;
        }
        else
        {
            Debug.Log("save doesnt exist");
            LoadManager.LevelToLoad = "menu";
            SceneManager.LoadScene("loading");
        }
    }

    public void UpdateInfos()
    {
        iceberg.text = qg.icebergStatus.ToString();
        floe.text = qg.floeStatus.ToString();
        sorbets.text = qg.sorbets.ToString();
    }

    public void EndMission()
    {
        chooseButton.interactable = false;
        playButton.interactable = false;
        selectedPenguins.Clear();
        LoadGame();
        isInPopUp = true;
        endMissionCanvas.SetActive(true);
    }

    public List<Penguin> GetRecruits()
    {
        List<Penguin> recruitList = new List<Penguin>();
        foreach(Penguin p in qg.penguins)
        {
            if (!p.IsInSquad())
            {
                recruitList.Add(p);
            }
        }
        return recruitList;
    }
    public List<Penguin> GetSquad()
    {
        List<Penguin> squad = new List<Penguin>();
        foreach (Penguin p in qg.penguins)
        {
            if (p.IsInSquad() && !p.IsDead())
            {
                squad.Add(p);
            }
        }
        return squad;
    }

    public List<Penguin> GetDeadPenguins()
    {
        List<Penguin> dead = new List<Penguin>();
        foreach (Penguin p in qg.penguins)
        {
            if (p.IsInSquad() && p.IsDead())
            {
                dead.Add(p);
            }
        }
        return dead;
    }

    public List<Mission> GetAvailableMissions()
    {
        List<Mission> missions = new List<Mission>();
        foreach (Mission m in qg.missions)
        {
            if (!m.IsDone())
            {
                missions.Add(m);
            }
        }
        return missions;
    }

    public List<Upgrade> GetUpgrades()
    {
        List<Upgrade> upgrades = new List<Upgrade>();
        foreach (Upgrade u in qg.upgrades)
        {
            upgrades.Add(u);
        }
        return upgrades;
    }

    public void SaveGame()
    {
        BinaryFormatter bf = new BinaryFormatter();
        if (!File.Exists("./saves"))
            System.IO.Directory.CreateDirectory("./saves");
        FileStream file = File.Create("./saves/doc.save");

        SerializedQG sQG = qg.Serialize();
        bf.Serialize(file, sQG);
        file.Close();
    }

    public void SaveAndQuit()
    {
        SaveGame();
        LoadManager.LevelToLoad = "menu";
        SceneManager.LoadScene("loading");
    }

    public int GetSorbets()
    {
        return qg.sorbets;
    }

    public bool Pay(int s)
    {
        if(qg.sorbets >= s)
        {
            qg.sorbets = qg.sorbets - s;
            sorbets.text = qg.sorbets.ToString();
            return true;
        } else
        {
            return false;
        }
    }

    public void SelectMission(Mission mission)
    {
        missionList.SetMissionsInteractable();
        chooseButton.interactable = true;
        selectedMission = mission;
    }

    public void CancelMission()
    {
        chooseButton.interactable = false;
        playButton.interactable = false;
        selectedPenguins.Clear();
        missionList.SetMissionsInteractable();
        squadList.SetPenguinsInteractable();
    }

    public bool SelectPenguin(Penguin penguin)
    {
        if (selectedPenguins.Count < 4)
        {
            selectedPenguins.Add(penguin);
            if (selectedPenguins.Count == 4)
            {
                playButton.interactable = true;
            }
            return true;
        } else
        {
            return false;
        }
    }

    public string GetSelectedMap()
    {
        return selectedMission.GetMap();
    }

    public List<Penguin> GetSelectedPenguins()
    {
        return this.selectedPenguins;
    }
}
