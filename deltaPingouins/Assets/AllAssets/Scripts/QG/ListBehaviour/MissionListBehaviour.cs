﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MissionListBehaviour : MonoBehaviour {

    public QGManager manager;
    public GameObject missionDetailPrefab;
    public GameObject parent;
    private GameObject[] elements;
    private List<Button> buttons = new List<Button>();

    public void Init()
    {
        List<Mission> missions = manager.GetAvailableMissions();
        int count = 0;
        float x = -455;
        foreach (Mission m in missions)
        {
            GameObject r = Instantiate(missionDetailPrefab, parent.transform);
            r.transform.localScale = new Vector3(1f, 1f, 1f);
            r.transform.localPosition = new Vector3(x, 0, 0);
            MissionButtonBehaviour script = r.GetComponent<MissionButtonBehaviour>();
            script.manager = manager;
            script.mission = m;
            Button button = r.GetComponent<Button>();
            buttons.Add(button);

            //set attributes
            Transform child = r.transform.Find("Name");
            Text name = child.GetComponent<Text>();
            child = r.transform.Find("Type");
            Text type = child.GetComponent<Text>();
            name.text = m.GetName();
            type.text = m.GetMissionType().ToString();

            // increment position and change parent size when needed
            x += 220;
            count += 220;
            if (count > 1150)
            {
                RectTransform rt = parent.GetComponent<RectTransform>();
                rt.sizeDelta += new Vector2(220f, 0);
                x -= 110;
            }
        }
        parent.transform.localPosition += new Vector3(count / 2, 0, 0);
    }

    public void Cancel()
    {
        RectTransform rt = parent.GetComponent<RectTransform>();
        rt.sizeDelta = new Vector2(1150f, 400f);
    }

    public void SetMissionsInteractable()
    {
        foreach(Button b in buttons)
        {
            b.interactable = true;
        }
    }
}