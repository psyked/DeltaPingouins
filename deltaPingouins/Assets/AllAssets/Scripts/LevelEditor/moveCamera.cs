﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveCamera : MonoBehaviour
{ 
    private Rigidbody2D rb2d;
    // Use this for initialization
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        float xAxisValue = Input.GetAxis("Horizontal") / 2;
        float yAxisValue = Input.GetAxis("Vertical") / 2;

        rb2d.position = new Vector2(rb2d.position.x + xAxisValue, rb2d.position.y + yAxisValue);

        var delta = Input.GetAxis("Mouse ScrollWheel");
        if (delta > 0 && GetComponent<Camera>().orthographicSize < 30)
            GetComponent<Camera>().orthographicSize++;
        if (delta < 0 && GetComponent<Camera>().orthographicSize > 4)
            GetComponent<Camera>().orthographicSize--;
    }
}
