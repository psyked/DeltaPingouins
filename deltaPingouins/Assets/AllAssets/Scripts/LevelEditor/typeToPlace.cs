﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum typeToPlace
{
    wall,
    obstacle,
    snow,
    ice,
    water,
    dirt,
    ennemy,
    hero
}
