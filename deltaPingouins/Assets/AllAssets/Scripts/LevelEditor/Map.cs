﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Map : MonoBehaviour {
    public GameObject[][] tiles;
    public string nameMap = "NewMap";
    public List<GameObject> Ennemies = new List<GameObject>();
    public List<GameObject> Heroes = new List<GameObject>();

    public GameObject FindFigtherAt(int x, int y, bool searchEnnemiesToo = true)
    {
        if(searchEnnemiesToo)
            foreach(GameObject fighter in Ennemies)
            {
                if (fighter.GetComponent<fighter>().getX() == x && fighter.GetComponent<fighter>().getY() == y)
                    return fighter;
            }
        foreach (GameObject fighter in Heroes)
        {
            if (fighter.GetComponent<fighter>().getX() == x && fighter.GetComponent<fighter>().getY() == y)
                return fighter;
        }
        return null;
    }
    
    public serialisableMap Serialize()
    {
        serialisableMap sMap = new serialisableMap();
        sMap.nameMap = this.nameMap;
        sMap.tiles = new typeToPlace[tiles.Length][];
        int x = 0;
        int y = 0;
        foreach (GameObject[] line in tiles)
        {
            sMap.tiles[x] = new typeToPlace[line.Length];
            foreach (GameObject tile in line)
            {
                sMap.tiles[x][y++] = (typeToPlace)tile.GetComponent<selectedTile>().typeTile;
            }
            x++;
            y = 0;
        }
        sMap.spawnEnnemies = new int[Ennemies.Count][];
        sMap.spawnHeroes = new int[Heroes.Count][];
        int i = 0;
        foreach (GameObject ennemy in Ennemies)
        {
            sMap.spawnEnnemies[i] = new int[2];
            sMap.spawnEnnemies[i][0] = (int)ennemy.GetComponent<fighter>().getX();
            sMap.spawnEnnemies[i][1] = (int)ennemy.GetComponent<fighter>().getY();
            i++;
        }
        i = 0;
        foreach (GameObject hero in Heroes)
        {
            sMap.spawnHeroes[i] = new int[2];
            sMap.spawnHeroes[i][0] = (int)hero.GetComponent<fighter>().getX();
            sMap.spawnHeroes[i][1] = (int)hero.GetComponent<fighter>().getY();
            i++;
        }

        return sMap;
    }
    public void Deserialize(serialisableMap sMap, boardInterface manager)
    {
        this.nameMap = sMap.nameMap;
        this.tiles = new GameObject[sMap.tiles.Length][];
        int x = 0;
        foreach (typeToPlace[] line in sMap.tiles)
        {
            int y = 0;
            this.tiles[x] = new GameObject[line.Length];
            foreach (typeToPlace typeTile in line)
            {
                GameObject toInstantiate = manager.getSnow();
                switch (sMap.tiles[x][y])
                {
                    case typeToPlace.ice:
                        toInstantiate = manager.getIce();
                        break;
                    case typeToPlace.dirt:
                        toInstantiate = manager.getDirt();
                        break;
                    case typeToPlace.water:
                        toInstantiate = manager.getWater();
                        break;
                    case typeToPlace.wall:
                        toInstantiate = manager.getWall();
                        break;
                    case typeToPlace.obstacle:
                        toInstantiate = manager.getObstacle();
                        break;
                    default:
                        toInstantiate = manager.getSnow();
                        break;
                }

                tiles[x][y] = Instantiate(toInstantiate, new Vector3(x, y, 0f), Quaternion.identity) as GameObject;

                tiles[x][y].GetComponent<selectedTile>().board = manager;
                tiles[x][y].GetComponent<selectedTile>().x = x;
                tiles[x][y].GetComponent<selectedTile>().y = y;
                tiles[x][y].GetComponent<selectedTile>().typeTile = (int)sMap.tiles[x][y];
                y++;
            }
            x++;
        }
        Ennemies = new List<GameObject>();
        Heroes = new List<GameObject>();
        foreach (int[] ennemyPosition in sMap.spawnEnnemies)
        {
            GameObject ennemyAdd = Instantiate(manager.getEnnemy(), new Vector3(ennemyPosition[0], ennemyPosition[1], 0f), Quaternion.identity) as GameObject;
            ennemyAdd.GetComponent<fighter>().setPosition(ennemyPosition[0], ennemyPosition[1]);
            Ennemies.Add(ennemyAdd);
        }
        foreach (int[] heroPosition in sMap.spawnHeroes)
        {
            GameObject heroAdd = Instantiate(manager.getHero(), new Vector3(heroPosition[0], heroPosition[1], 0f), Quaternion.identity) as GameObject;
            heroAdd.GetComponent<fighter>().setPosition(heroPosition[0], heroPosition[1]);
            Heroes.Add(heroAdd);
        }
    }
}
