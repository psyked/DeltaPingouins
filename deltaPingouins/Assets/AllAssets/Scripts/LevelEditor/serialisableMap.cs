﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class serialisableMap
{
    public string nameMap = "NewMap";
    public typeToPlace[][] tiles;
    public int[][] spawnEnnemies;
    public int[][] spawnHeroes;
}
