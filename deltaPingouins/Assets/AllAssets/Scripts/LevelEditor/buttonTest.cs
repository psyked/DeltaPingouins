﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class buttonTest : MonoBehaviour
{
    public Dropdown maps;

    private List<string> mapsList;
    
    public void TaskOnClick()
    {
        if (maps.options[maps.value].text.Equals(""))
            return;
        LoadManager.previousLevel = "levelEditor";
        LoadManager.LevelToLoad = "mission";
        LoadManager.mapName = maps.options[maps.value].text;
        SceneManager.LoadScene("loading"); ;
    }
}
