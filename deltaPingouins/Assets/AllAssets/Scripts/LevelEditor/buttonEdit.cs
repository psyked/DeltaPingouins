﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class buttonEdit : MonoBehaviour {

    public Dropdown maps;
    public GameObject board;

    private List<string> mapsList;
 
    private void Start () {
        Button btn = GetComponent<Button>();
        btn.onClick.AddListener(TaskOnClick);

        maps.ClearOptions();

        try
        {
            foreach (string file in System.IO.Directory.GetFiles("./maps/", "*.map"))
            // get list of files *.maps in folder
            {
                maps.options.Add(new Dropdown.OptionData() { text = Path.GetFileName(file) });
                maps.captionText.text = "Select Map";
            }
        }
        catch(Exception excp)
        {
            Debug.Log("no maps folder");
            maps.options.Add(new Dropdown.OptionData() { text = "No" });
            maps.options.Add(new Dropdown.OptionData() { text = "Map" });
            maps.options.Add(new Dropdown.OptionData() { text = "Found" });

            maps.captionText.text = "No Map found";
        }        
    }

    private void TaskOnClick()
    {
        board.GetComponent<boardManager>().loadBoard(maps.options[maps.value].text);
    }
}
