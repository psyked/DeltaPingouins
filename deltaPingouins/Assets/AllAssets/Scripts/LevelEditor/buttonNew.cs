﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class buttonNew : MonoBehaviour {

    public InputField length;
    public InputField heigh;
    public InputField nameMap;

    public GameObject board;

    private void Awake()
    {
        Button btn = GetComponent<Button>();
        btn.onClick.AddListener(TaskOnClick);
    }
    private void TaskOnClick()
    {
        int x = 50;
        int y = 25;
        string name = "New map";

        int.TryParse(length.text, out x);
        int.TryParse(heigh.text, out y);
        if (x < 10) x = 10;
        if (y < 10) y = 10;
        if (nameMap.text.Length > 2)
            name = nameMap.text;

        board.GetComponent<boardManager>().createBoard(x, y, name);
    }
}
