﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class selectedTile : MonoBehaviour {

    public boardInterface board;
    public  SpriteRenderer spriteRenderer;
    public int x { get; set; }
    public int y { get; set; }

    public int typeTile = 0;

    private Color baseColor;

    public void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        baseColor = spriteRenderer.color;
    }

    public void OnMouseEnter()
    {        
        board.enterTile(x, y);
    }
    public void OnMouseExit()
    {
        board.exitTile(x, y);
    }
    public void OnMouseUpAsButton()
    {
        board.ClickTile(x, y);
    }
    
    public void resetColor()
    {
        spriteRenderer.color = baseColor;
    }
}
