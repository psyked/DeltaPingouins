﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class imageFighter : MonoBehaviour {
    //public GameObject canvasInfo;
    public Text text;
    public Image image;

    private GameObject fighter = null;

    public void setFighter(GameObject f)
    {
        fighter = f;
        UpdateTextDescription();
    }

    public void UpdateTextDescription()
    {
        text.text = fighter.GetComponent<fighter>().GetDescription(true);
        image.sprite = fighter.GetComponent<fighter>().ImageInfo;// fighter.GetComponent<SpriteRenderer>().sprite;
    }

    /*
     *  add rigidbody and boxcollider to use mouse event : are they needed ?
     *  
    private void OnMouseUpAsButton()
    {
        if(fighter != null)
            canvasInfo.GetComponent<InfoFighter>().SetFighter(this.fighter);
    }*/
}
