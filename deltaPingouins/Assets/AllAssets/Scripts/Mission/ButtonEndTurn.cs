﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonEndTurn : MonoBehaviour {
    public GameObject manager;

    public void onClick()
    {
        manager.GetComponent<BoardGameManager>().ChangeTurn();
    }
	
}
