﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class buttonReturnMenu : MonoBehaviour {

    public void OnClick()
    {
        LoadManager.LevelToLoad = LoadManager.previousLevel;
        LoadManager.previousLevel = "mission";
        SceneManager.LoadScene("loading");
    }
}
