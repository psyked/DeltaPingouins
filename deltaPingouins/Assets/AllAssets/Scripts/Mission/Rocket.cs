﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rocket : MonoBehaviour {

    public BoardGameManager manager = null;
    public Vector2 destination;
    public int damage = 3;
    public int VelocityRocket = 6;

    public ParticleSystem explosion;

    private Rigidbody2D rb2d;

	void Awake () {
        rb2d = GetComponent<Rigidbody2D>();
	}

    public void SetDestination(Vector2 coords)
    {
        destination = coords;

        float diffX = destination.x - rb2d.position.x;
        float diffY = destination.y - rb2d.position.y;
        
        rb2d.velocity = new Vector2(diffX, diffY).normalized * VelocityRocket;
    }
	
	void Update () {
		if((Mathf.Abs(destination.x - rb2d.position.x) < 0.2) && (Mathf.Abs(destination.y - rb2d.position.y) < 0.2))
        {
            ParticleSystem blast = Instantiate(explosion, new Vector3(rb2d.position.x, rb2d.position.y, 0f), Quaternion.identity);
            UnityEngine.Object.Destroy(blast, 2);

            manager.RocketExplosion(destination, damage);
        }
	}
}
