﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfoFighter : MonoBehaviour {

    public GameObject Image;
    public GameObject NameFighter;
    public GameObject DescriptionFighter;
    public GameObject ImageContainer;

    private GameObject fighter;
    private bool initialised = false; 

    private void Awake()
    {
        ImageContainer.SetActive(false);
    }

    public void SetFighter(GameObject fighter)
    {
        if (!initialised)
        {
            initialised = true;
            ImageContainer.SetActive(true);
        }
        this.fighter = fighter;
        ReloadInfo();
    }

    public void ReloadInfo()
    {
        fighter fighterClass = fighter.GetComponent<fighter>();
        NameFighter.GetComponent<Text>().text = fighterClass.Name;
        DescriptionFighter.GetComponent<Text>().text = fighterClass.GetDescription();
        Image.GetComponent<Image>().sprite = fighterClass.ImageInfo;
        //Image.GetComponent<Image>().sprite = fighter.GetComponent<SpriteRenderer>().sprite;// todo : change with : fighterClass.ImageInfo;
    
    }

}
