﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySound : MonoBehaviour {

    public AudioClip start;
    public AudioClip win;
    public AudioClip lose;
    public AudioClip punch;
    public AudioClip splash;
    public AudioClip explosion;
    public AudioClip kill;
    public AudioClip flee;
    public AudioClip theme;

    public float volume = 1;

    private AudioSource audioS;

    private void playSound(AudioClip clip, float vol = 1, int time = 3)
    {
        AudioSource audio = gameObject.AddComponent<AudioSource>();
        audio.PlayOneShot(clip, volume * vol);
        UnityEngine.Object.Destroy(audio, time);
    }

    private void Awake()
    {
        Play("start");
        audioS = GetComponent<AudioSource>();
        audioS.loop = true;
        audioS.clip = theme;
        audioS.Play();
    }

    public void Play (string sound)
    {
        switch (sound)
        {
            case "start":
                AudioSource audio = gameObject.AddComponent<AudioSource>();
                audio.PlayOneShot(start, volume);
                UnityEngine.Object.Destroy(audio, 3);
                break;
            case "win":
                playSound(win,1, 15);
                break;
            case "lose":
                playSound(lose,0.6F, 15);
                break;
            case "punch":
                playSound(punch, 1.2F);
                break;
            case "splash":
                playSound(splash, 1.5F);
                break;
            case "explosion":
                playSound(explosion);
                break;
            case "flee":
                playSound(flee);
                break;
            case "kill":
                playSound(kill);
                break;
        }
    }

}
