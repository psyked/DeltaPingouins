﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FighterSelectedInfo : MonoBehaviour {

    public Text text;
    public Image image;

    private GameObject fighter = null;
	
    public void setFighter(GameObject f)
    {
        fighter = f;
    }

	void Update ()
    {
        if (fighter == null)
            return;

        text.text = fighter.GetComponent<fighter>().GetDescription();
        image.sprite = fighter.GetComponent<fighter>().ImageInfo;
    }
}
