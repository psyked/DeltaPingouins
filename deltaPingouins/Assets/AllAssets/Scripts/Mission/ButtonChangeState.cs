﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonChangeState : MonoBehaviour {

    public GameObject manager;
    public int newState = 0;

    public void OnClick()
    {
        manager.GetComponent<BoardGameManager>().ChangeState(newState);
    }
	
}
