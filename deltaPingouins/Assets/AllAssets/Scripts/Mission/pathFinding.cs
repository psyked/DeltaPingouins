﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pathFinding {

    static public float Distance(Vector2 a, Vector2 b)
    {
        return Mathf.Sqrt(Mathf.Abs(a.x - b.x) * Mathf.Abs(a.x - b.x)
                + Mathf.Abs(a.y - b.y) * Mathf.Abs(a.y - b.y));
    }

    static public Vector2[] walkableNeighbors(Vector2 point, bool[][] map)
    {
        List<Vector2> neighbors = new List<Vector2>();

        if (point.x - 1 >= 0)
            if (map[(int)point.x - 1][(int)point.y])
                neighbors.Add(new Vector2(point.x - 1, point.y));

        if (point.x + 1 < map.Length)
            if (map[(int)point.x + 1][(int)point.y])
                neighbors.Add(new Vector2(point.x + 1, point.y));

        if (point.y - 1 >= 0)
            if (map[(int)point.x][(int)point.y - 1])
                neighbors.Add(new Vector2(point.x, point.y - 1));

        if (point.y + 1 < map[(int) point.x].Length)
            if (map[(int)point.x][(int)point.y + 1])
                neighbors.Add(new Vector2(point.x, point.y + 1));

        return neighbors.ToArray();
    }

	static public Vector2[] findPath(Vector2 start, Vector2 end, bool[][] walkableMap, int sizePathMax = 3)
    {
        List<Vector2> path = new List<Vector2>();
        path.Add(start);
        int index = 0;
        Vector2 actual = start;
        float shortestDistance;
        Vector2 closestNeighbor = new Vector2();

        int debug = 0;

        while(actual != end && debug < sizePathMax * 5)
        {
            debug++;
            if (path.Count > sizePathMax)
                return null;

            shortestDistance = -1;

            Vector2[] neighbors = walkableNeighbors(actual, walkableMap);
            if (neighbors.Length == 0)
                return null;

            foreach (var neighbor in neighbors)
            {
                if(Distance(neighbor, end) <= shortestDistance || shortestDistance < 0)
                {
                    closestNeighbor = neighbor;
                    shortestDistance = Distance(neighbor, end);
                }
            }

            //Debug.Log("closest neighbor : " + closestNeighbor.x + " " + closestNeighbor.y);
            //Debug.Log("list :");
            //foreach (Vector2 step in path)
            //   Debug.Log(step.x + " " + step.y);

            if (Distance(actual, end) == 1 && shortestDistance > 1)
            {  // means the end point isn't walkable
                //Debug.Log("unreachable");
                return path.ToArray();
            }

            if (shortestDistance == Distance(actual, end))
            {  // if we can't move 
                //Debug.Log("trapped");

                if (Distance(actual, end) == 1)
                {  // means the end point isn't walkable
                     //Debug.Log("unreachable");
                    return path.ToArray();
                }

                //Debug.Log("index : " + index);
                path.Remove(path[index]);
                actual = path[--index];
                walkableMap[(int)actual.x][(int)actual.y] = true;
                walkableMap[(int)closestNeighbor.x][(int)closestNeighbor.y] = false;
            }
            else
            {
                index++;
                walkableMap[(int)actual.x][(int)actual.y] = false;
                actual = closestNeighbor;
                path.Add(actual);
            }
        }

        if (debug == sizePathMax * 5)
            return null;

        return path.ToArray();
    }
}