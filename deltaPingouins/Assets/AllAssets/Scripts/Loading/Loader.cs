﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Loader : MonoBehaviour {

    public float speedX = 1; 
    public float speedY = 0;
    public float maxX = 1;
    public float maxY = 1;
    public int sleepTime = 1;

    private bool isLaunchingScene = false;
    private Rigidbody2D rb2d;

    private void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        rb2d.velocity = (new Vector2(speedX, speedY));
        StartCoroutine(LoadScene());
    }

    private void Update()
    {
        /*if (Input.GetKeyDown(KeyCode.Space))
        {
            rb2d.velocity = new Vector2(0, 0);
        }*/

        if (speedX < 0)
            if (rb2d.position.x <= maxX)
            {
                rb2d.position = (new Vector2(maxX, rb2d.position.y));
                rb2d.velocity = (new Vector2(0, rb2d.velocity.y));
            }
        if (speedX > 0)
            if (rb2d.position.x >= maxX)
            {
                rb2d.position = (new Vector2(maxX, rb2d.position.y));
                rb2d.velocity = (new Vector2(0, rb2d.velocity.y));
            }

        if (speedY < 0)
            if (rb2d.position.y <= maxY)
            {
                rb2d.position = (new Vector2(rb2d.position.x, maxY));
                rb2d.velocity = (new Vector2(rb2d.velocity.x, 0));
            }
        if (speedY > 0)
            if (rb2d.position.y >= maxY)
            {
                rb2d.position = (new Vector2(rb2d.position.x, maxY));
                rb2d.velocity = (new Vector2(rb2d.velocity.x,0));
            }
        if (rb2d.velocity.y == 0 && rb2d.velocity.x == 0)
        {
            if(!isLaunchingScene)
                StartCoroutine(LoadScene());
        }
    }
    IEnumerator LoadScene()
    {
        isLaunchingScene = true;
        yield return new WaitForSeconds(sleepTime);
        AsyncOperation async = SceneManager.LoadSceneAsync(LoadManager.LevelToLoad);
        while (!async.isDone)
        {
            yield return null;
        }
    }
}
